This repository was forked from https://github.com/msieg/deep-music-visualizer for educational purposes as part of the Digital and Soft Skills for Inerdisciplinary Projects Development (DSSIPD) course in the RWTH university, Aachen.

# Deep Music Visualizer
Want to make a deep music video? Wrap your mind around BigGAN. Developed at Google by Brock et al. (2018)¹, BigGAN is a recent chapter in a brief history of generative adversarial networks (GANs). 
BigGAN is considered Big because it contains over 300 million parameters trained on hundreds of google TPUs at the cost of an estimated $60,000. The result is an AI model that generates images from 1128 input parameters:

# Google colaboratory
For this work i've used local(CPU) and CUDA on Google Colab.
The [Deep Visualizer Jupyter](Deep_Visualizer.ipynb) file in first steps specifies setting up local directory and installing pre-reqs.
The experimentation are described in Section 2,3,4.

## Installation

This repo has been tested on Python3  
Scipy 1.1.2 Version also works

Assuming you have python installed, open terminal and run these commands:

```bash
git clone https://gitlab.com/khawajaali/deep-music-visualizer.git
cd deep-music-visualizer
pip install -r requirements.txt
```


## How to run

All features of the visualizer are available as input parameters. Each parameter is described below.

### Song File Types

Audio file of type mp3, wav, or ogg.

This is the only required argument!

Example:

```bash
python visualize.py --song MiGente.wav
```

What’s going on here? The deep music visualizer syncs pitch with the class vector and volume and tempo with the noise vector, so that pitch controls the objects, shapes, and textures in each frame, while volume and tempo control movement between frames. At each time point in the song, a chromagram of the twelve chromatic notes determines the weights {0 ≤ 1} of up to twelve ImageNet classes in the class vector. Independently, the rate of change of the volume — mainly percussion — controls the rate of change of the noise vector.

# New Implemenataion
## Export as Gif

You can export the video also as a GIF
default: 0
set to 1 to generate Gif

Example:

```bash
%run visualize.py --song MiGente.wav --generate_gif 1
```

## start time

You can now specify the starting of the audio file by setting the _start_time_ value.
default: 0 _will always start from start_
set any value in seconds

Example:

```bash
%run visualize.py --song MiGente.wav --start_time 30
```

## Audio File Graphs

You can now specify the starting of the audio file by setting the _start_time_ value.
default: 0 _no graph created_
Range: 1 ≤ 4
Set Option for the following:
1. Chromagram with pitch classes
2. Linear-frequency power spectrogram
3. Tempogram
4. Beat-synchronous chroma in natural time

Example:

```bash
%run visualize.py --song MiGente.wav --graph 2
```
Graphs look like following
![Chromagram with pitch classes](data/Chromagram.JPG)

## Video Customization (improvement)

128, 256, or 512 (if you specify any value except this, 128 is set to avoid failure)

Default: 512

If you are running on a CPU (if you're not sure, you are on a CPU), you might want to use a lower resolution or else the code will take a very long time to run. At 512X512, it will take ~7 HOURS to generate 1 minute of video on a standard desktop computer (assuming all other parameters are default). At 128x128, it would take ~25 minutes. To speed up runtime, you can decrease the resolution or increase the [frame_length](#Frame_length). 

To dramatically speed up runtime and generate higher quality videos, use a resolution of 512 on a [GPU with Google Colab](https://research.google.com/colaboratory/).

### Running on Google Colab

Example:

```bash
%run visualize.py --song MiGente.wav --resolution 128
```

### duration

Duration of the video output in seconds. It can be useful to generate shorter videos while you are tweaking the other visualizer parameters. Once you find your preferred parameters, remove the duration argument and set [use_previous_vectors](#use_previous_vectors) to 1 to generate the same video but for the full duration of the song. 

Default: Full length of the audio

Example:

```bash
%run visualize.py --song MiGente.wav --duration 30
```

### pitch sensitivity

The pitch sensitivity controls how rapidly the class vector (thematic content of the video) will react to changes in pitch. The higher the number, the higher the sensitivity. 

Range: 1 – 299

Recommended range: 200 – 295

Default: 220

Example:

```bash
%run visualize.py --song MiGente.wav --pitch_sensitivity 280
```

### tempo sensitivity

The tempo sensitivity controls how rapidly the noise vector (i.e. the overall size, position, and orientation of objects in the images) will react to changes in volume and tempo. The higher the number, the higher the sensitivity. 

Recommended range: 0.05 – 0.8

Default: 0.25
Range: 0 ≤ 1

Example:

```bash
%run visualize.py --song MiGente.wav --tempo_sensitivity 0.1
```

### depth

The depth specifies the max value of the class vector units. Numbers closer to 1 seem to yield more thematically rich content. Numbers closer to 0 seem to yield more 'deep' structures like human and dog faces. However, this depends heavily on the specific classes you are using.  

Range: 0.01 - 1

Default: 1

Example:

```bash
%run visualize.py --song MiGente.wav --depth 0.5
```

### classes

If you want to choose which classes (image categories) to visualize, you can specify a list of ImageNet indices (1-1000) here. ([list of ImageNet class indices](https://gist.github.com/yrevar/942d3a0ac09ec9e5eb3a)). The number of classes must be equal to [num_classes] (default is twelve, corresponding to the twelve musical pitches (A, A#, B, etc.)). You can also enter the class indices in order of priority (highest priority first) and set [sort_classes_by_power](#sort_classes_by_power) to 1. 

Default: Twelve random indices between 0-999

Example (if num_classes is set to default of twelve):
```bash
%run visualize.py --song MiGente.wav --classes 45 99 567 234 89 90 105 998 56 677 884 530
```

### num_classes

If you want to focus the visualizer around fewer than twelve themes, you can set num_classes to a number less than twelve. Since each class is associated with a pitch, the pitches that are retained when num_classes < 12 are those with the most overall power in the song. 

Default: 12

Example:
```bash
%run visualize.py --song MiGente.wav --num_classes 4 
```

Or if you want to choose the classes:

```bash
%run visualize.py --song MiGente.wav --num_classes 4 --classes 987 23 56 782
```

### sort_classes_by_power

Set this to 1 if you want to prioritize the classes based on the order that you entered them in the [class input](#classes). If you do not specify the class input, there is no reason to set this to 1. If you do specify the class input and do not set this to 1, the classes will be associated with the pitches in harmonic order from A, A#, B, etc.

Example:

```bash
%run visualize.py --song MiGente.wav  --classes 45 99 567 234 89 90 105 998 56 677 884 530 --sort_classes_by_power 1
```

### jitter

The jitter prevents the same exact noise vectors from cycling repetitively during repetitive music so that the video output is more interesting. If you do want to cycle repetitively, set jitter to 0.

Range: 0 – 1
Default: 0.5 

Example:

```bash
%run visualize.py --song MiGente.wav --jitter 0
```

### frame_length

The frame length controls the number of audio frames per video frame in the output. If you want a higher frame rate for visualizing very rapid music, lower the frame_length. If you want a lower frame rate (perhaps if you are running on a CPU and want to cut down your runtime), raise the frame_length. The default of 512 is high quality. 

Range: Multiples of 2^6

Default: 512

Example:

```bash
%run visualize.py --song MiGente.wav --frame_length 2048
```

### truncation

The truncation controls the variability of images that BigGAN generates by limiting the max values in the noise vector. Truncations closer to 1 yield more variable images, and truncations closer to 0 yield simpler images with more recognizable, normal-looking objects. 

Range: 0.1 - 1

Default: 1

Example:

```bash
%run visualize.py --song MiGente.wav --truncation 0.4
```

### smooth_factor

After the class vectors have been generated, they are smoothed by interpolating linearly between the means of class vectors in bins of size [smooth_factor]. This is performed because small local fluctuations in pitch can cause the video frames to fluctuate back and forth. If you want to visualize very fast music with rapid changes in pitch, you can lower the smooth factor. You may also want to lower the frame_length in that case. However, for most songs, it is difficult to avoid rapid fluctuations with smooth factors less than 10. 

Range: > 0

Recommended range: 10 – 30

Default: 20

Example:

```bash
%run visualize.py --song MiGente.wav --smooth_factor 6
```

### batch_size

BigGAN generates the images in batches of size [batch_size]. The only reason to reduce batch size from the default of 30 is if you run out of CUDA memory on a GPU. Reducing the batch size will slightly increase overall runtime. 

Default: 30

Example:

```bash
%run visualize.py --song MiGente.wav --batch_size 20
```

### use_previous_classes

If your previous run of the visualizer used random classes (i.e. you did not manually set the [class](#classes) input), and you liked the video output but want to mess with some other parameters, set use_previous_classes to 1 so that you create a similar video with the same classes on the next run of the code. 

Default: 0

Example:

```bash
%run visualize.py --song MiGente.wav --use_previous_classes 1
```

### use_previous_vectors

If you're messing around with the visualizer parameters, it can be useful to generate videos with short durations to keep your runtime low. Once you find the right set of parameters, remove the [duration](#duration) argument and set use_previous_vectors to 1 to generate the same video again, but for a longer duration. 

Default: 0

Example:

```bash
%run visualize.py --song MiGente.wav --use_previous_vectors 1
```

### output_file

Output file name

Default: output.mp4

Example:

```bash
%run visualize.py --song MiGente.wav --output_file my_movie.mp4
```

